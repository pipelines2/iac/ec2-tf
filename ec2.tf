resource "aws_instance" "al2_demo" {
  ami           = "ami-09538990a0c4fe9be" #Amazon Linux 2 AMI x86
  instance_type = "t3a.micro"

  security_groups = [aws_security_group.ec2_sg.id]
  key_name        = aws_key_pair.key_pair.key_name

  #user_data = <<EOF
  #  #! /bin/bash
  #  sudo su
  #  sudo yum update
  #  sudo yum install -y httpd
  #  sudo chkconfig httpd on
  #  sudo service httpd start
  #  echo "<h1>Deployed EC2 With Terraform</h1>" | sudo tee /var/www/html/index.html
  #  sudo yum install docker
  #  sudo usermod -a -G docker ec2-user
  #  id ec2-user
  #  newgrp docker
  #  EOF

  user_data = <<EOF
    #! /bin/bash
    sudo su
    sudo yum update
    sudo amazon-linux-extras install nginx1 -y
    sudo systemctl enable nginx
    sudo systemctl start nginx
    echo "<h1>Deployed EC2 With Terraform</h1>" | sudo tee /usr/share/nginx/html/index.html
    sudo yum install docker
    sudo usermod -a -G docker ec2-user
    id ec2-user
    newgrp docker
    EOF

  tags = {
    Name = "al2_demo"
  }
}