# Below Code will generate a secure private key with encoding
resource "tls_private_key" "key_pair" {
  algorithm = "RSA"
  rsa_bits  = 4096
}
# Create the Key Pair
resource "aws_key_pair" "key_pair" {
  key_name   = "al2-key-pair"
  public_key = tls_private_key.key_pair.public_key_openssh

  provisioner "local-exec" {
    command = <<-EOT
      echo ${tls_private_key.key_pair.private_key_pem}
    EOT
  }
}
# Save file
#resource "local_file" "ssh_key" {
#  filename = "${aws_key_pair.key_pair.key_name}.pem"
#  content  = tls_private_key.key_pair.private_key_pem
#}